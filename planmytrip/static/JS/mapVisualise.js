function initMap() {

  var map = new google.maps.Map(document.getElementById('map'), {

    center: {
      lat: parseFloat(parseFloat(lat)),
      lng: parseFloat(parseFloat(lng))
    },
    zoom: 13,
    mapTypeId: 'roadmap'

  });
  marker = new google.maps.Marker({
    position: {
      lat: parseFloat(parseFloat(lat)),
      lng: parseFloat(parseFloat(lng))
    },
    map: map
  });
}



function likethis() {
  $(".button-like").toggleClass("liked")
  $.ajax({
    url: '/planmytrip/ajax/like_attraction/',
    data: {
      'liked_attraction': attraction_name_slug
    },
    dataType: 'json',
    success: function(data) {
      if (data.success) {}
    }
  });
}

function unlikethis() {
  $(".button-like").toggleClass("liked")
  $.ajax({
    url: '/planmytrip/ajax/unlike_attraction/',
    data: {
      'liked_attraction': attraction_name_slug
    },
    dataType: 'json',
    success: function(data) {
      if (data.success) {}
    }
  });
}




// var marker = new google.maps.Marker({
//   position: myLatLng,
//   map: map,
//   title: 'Hello World!'
// });