// function mymethod(event) {
//   var select = event.target.value;
//   alert(select);
// }

// https://stackoverflow.com/questions/41273295/django-view-not-rendering-after-ajax-redirect/41273337

function getEverything() {
  var option = $('#choice').find(":selected").text();
  var filter = $('#select-options').find(":selected").text();

  query = {
    option: option,
    filter: filter
  }

  $.ajax({
    url: '/planmytrip/ajax/search_filter/',
    data: query,
    dataType: 'json',
    success: function(data) {
      if (data.success) {
        window.location.href = data.url;
      }
    }
  });

}



function getOptions() {
  var option = $('#choice').find(":selected").text();
  console.log("Option selected is: " + option);
  $.ajax({
    url: '/planmytrip/ajax/get_options/',
    data: {
      option: option
    },
    dataType: 'json',
    success: function(data) {
      $('#search-options').html("");
      console.log(data);
      options = JSON.parse(data)
      console.log(options);
      var searchTag = document.createElement("SELECT");
      searchTag.setAttribute("class", "form-control")
      searchTag.setAttribute("id", "select-options")
      for (var i in options) {
        var optionTag = document.createElement("OPTION");
        console.log(option);
        optionTag.setAttribute('value', options[i]);
        var optionText = document.createTextNode(options[i]);
        optionTag.appendChild(optionText);
        searchTag.appendChild(optionTag);
      }
      $('#search-options').append(searchTag)
    }
  });
}

function searchByAttraction() {
  var filter = $('#advSearch').val();
  console.log(filter);


  $.ajax({
    url: '/planmytrip/ajax/search_attraction/',
    data: {
      filter: filter
    },
    dataType: 'json',
    success: function(data) {
      if (data.success) {
        window.location.href = data.url;
      }
    }
  });
}