var marker;

function initAutocomplete() {
  console.log(marker);

  var map = new google.maps.Map(document.getElementById('map'), {
    center: {
      lat: -33.8688,
      lng: 151.2195
    },
    zoom: 13,
    mapTypeId: 'roadmap'
  });

  // Create the search box and link it to the UI element.
  var input = document.getElementById('pac-input');
  var searchBox = new google.maps.places.SearchBox(input);
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

  // Bias the SearchBox results towards current map's viewport.
  map.addListener('bounds_changed', function() {
    searchBox.setBounds(map.getBounds());
  });


  var markers = [];
  // Listen for the event fired when the user selects a prediction and retrieve
  // more details for that place.
  searchBox.addListener('places_changed', function() { //looks for something online
    var places = searchBox.getPlaces();

    if (places.length == 0) {
      return;
    }

    // Clear out the old markers.
    markers.forEach(function(multi_marker) {
      multi_marker.setMap(null);
    });
    markers = [];

    // For each place, get the icon, name and location.
    var bounds = new google.maps.LatLngBounds();
    if (places.length == 1) {
      setMarker(places[0].geometry.location, map)
      marker = new google.maps.Marker({
        position: places[0].geometry.location,
        map: map
      });

    }
    places.forEach(function(place) {
      if (!place.geometry) {
        console.log("Returned place contains no geometry");
        return;
      }

      // Create a marker for each place.
      markers.push(new google.maps.Marker({
        map: map,
        title: place.name,
        position: place.geometry.location
      }));

      if (place.geometry.viewport) {
        // Only geocodes have viewport.
        bounds.union(place.geometry.viewport);
      } else {
        bounds.extend(place.geometry.location);
      }
    });
    map.fitBounds(bounds);
  });

  function placeMarker(location) {
    markers.forEach(function(multi_marker) {
      multi_marker.setMap(null);
    });
    markers = [];
    if (marker) {
      marker.setPosition(location);
    } else {
      // console.log(location.Lat);
      marker = new google.maps.Marker({
        position: location,
        map: map
      });
    }
    setMarker(location, map)
  }

  google.maps.event.addListener(map, 'click', function(event) {
    placeMarker(event.latLng);
  });
}

function getLoc() {
  console.log(marker.position.lat());
}

var infowindow

function setMarker(location, map) {
  $("#id_position_0").attr('value', location.lat());
  $("#id_position_1").attr('value', location.lng());

  var geocoder = new google.maps.Geocoder;

  var latlng = {
    lat: location.lat(),
    lng: location.lng()
  };

  // var test = forms.AttractionForm

  geocoder.geocode({
    'location': latlng
  }, function(results, status) {
    if (status === 'OK') {
      if (results[0]) {
        map.setZoom(11);
        if (marker) {
          marker.setPosition(latlng);
        } else {
          // console.log(location.Lat);
          marker = new google.maps.Marker({
            position: latlng,
            map: map
          });
        }

        if (infowindow) {
          infowindow.setContent(results[0].formatted_address)
        } else {
          infowindow = new google.maps.InfoWindow;
          infowindow.setContent(results[0].formatted_address);
        }
        // infowindow.setContent(results[0].formatted_address);
        console.log(results[0])
        console.log(results[0].address_components)
        console.log(results[0].address_components.length)
        var province
        var region
        for (var index = 0; index < results[0].address_components.length; index++) {
          if (results[0].address_components[index].types.includes("administrative_area_level_2")) {
            province = results[0].address_components[index].long_name
          }
          if (results[0].address_components[index].types.includes("administrative_area_level_1")) {
            region = results[0].address_components[index].long_name
          }
        }
        $('#id_region').attr('value', region)
        $('#id_province').attr('value', province)
        infowindow.open(map, marker);
      } else {
        window.alert('No results found');
      }
    } else {
      window.alert('Geocoder failed due to: ' + status);
    }
  });
  // var region = location.

}