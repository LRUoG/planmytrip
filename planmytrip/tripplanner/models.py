from django.db import models
from django.template.defaultfilters import slugify
from django.core.validators import MaxValueValidator, MinValueValidator
from django.contrib.auth.models import User
from geoposition.fields import GeopositionField
from django.db.models.signals import post_save
from django.dispatch import receiver




class Attraction(models.Model):
    CATEGORY_CHOICES = (
        ('Nature', 'Nature'),
        ('Sport', 'Sport'),
        ('History', 'History'),
    )
    name = models.CharField(max_length=30, unique=True)
    region = models.CharField(max_length=30)
    province = models.CharField(max_length=128)
    category = models.CharField(max_length=30, choices=CATEGORY_CHOICES)
    picture = models.ImageField(upload_to='attractions')
    position = GeopositionField()
    description= models.TextField(max_length=600)
    slug = models.SlugField(unique=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Attraction, self).save(*args, **kwargs)

    def __str__(self):
        return self.name


class Profile(models.Model):
    user = models.OneToOneField(User, related_name = "profile")
    picture = models.ImageField(upload_to='profile', default='profile/default-icon.png')
    liked_attractions= models.ManyToManyField(Attraction, related_name="liked_attractions")

    def __str__(self):
        return self.user.username

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()


class Review(models.Model):
    RATING_CHOICES = (
        (1, 1),
        (2, 2),
        (3, 3),
        (4, 4),
        (5, 5),
    )
    date = models.DateField(auto_now=True)
    text = models.CharField(max_length=600)
    attraction = models.ForeignKey('tripplanner.Attraction', related_name= 'reviews')
    user = models.ForeignKey(User, related_name= 'reviews')
    appeal = models.IntegerField(validators=[MaxValueValidator(
        5), MinValueValidator(1)], choices=RATING_CHOICES, default=1)
    price = models.IntegerField(validators=[MaxValueValidator(
        5), MinValueValidator(1)], choices=RATING_CHOICES, default=1)
    ease_of_access = models.IntegerField(validators=[MaxValueValidator(
        5), MinValueValidator(1)], choices=RATING_CHOICES, default=1)

    def __str__(self):
        return self.text
