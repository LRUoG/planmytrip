from django.shortcuts import render
from tripplanner.models import Attraction, Profile, Review
from tripplanner.forms import UserForm, ProfileForm, AttractionForm, ReviewForm, EditProfileForm, EditUserForm
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse, QueryDict
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.db.models import Max, F, FloatField, Sum, Count, Avg
from django.contrib.auth.models import User
import json
import operator


def home(request):
    # query database for Attrction in the given categories
    history_list = Attraction.objects.filter(category="history")
    sport_list = Attraction.objects.filter(category="sport")
    nature_list = Attraction.objects.filter(category="nature")
    context_dict = {'history': history_list,
                    'sport': sport_list,
                    'nature': nature_list}
    return render(request, 'planmytrip/home.html',
                      context=context_dict)


def most_popular(request):
    best_appeal = {}
    most_liked = {}
    reviews_count = {}
    # checks is any attraction exist
    if Attraction.objects.all():
        # query all reviews for each attraction and makes an average
        for attraction in Attraction.objects.all():
            appeal = attraction.reviews.all().aggregate(Avg('appeal'))
            best_appeal.update({attraction : appeal['appeal__avg']})
            most_liked[attraction] = attraction.liked_attractions.all().count()
            reviews_count[attraction] = attraction.reviews.all().count()
        res = {k:v for k,v in best_appeal.items() if v is not None}
        # gets the maximum from the list of all attractions
        appeal_attraction = max(res.items(), key=operator.itemgetter(1))[0]
        liked_attraction = max(most_liked.items(), key=operator.itemgetter(1))[0]
        reviews_attraction = max(reviews_count.items(), key=operator.itemgetter(1))[0]

        context_dict = {'best_appeal': appeal_attraction,
                   'most_liked' : liked_attraction,
                   'most_reviewed': reviews_attraction,
                    }
    else:
        context_dict = {}

    return render(request, 'planmytrip/most_popular.html',
                      context=context_dict)



@login_required
# this function creates a form for adding an attraction
def add_attraction(request):
    # if the request method is a post the form is checked and saved
    if request.method == 'POST':
        attraction_form = AttractionForm(request.POST, request.FILES)
        if attraction_form.is_valid():
            attraction = attraction_form.save(commit=False)
            attraction.save()
            return home(request)
        else:
            print(attraction_form.errors)
    else:
        attraction_form = AttractionForm()
    return render(request, 'planmytrip/add_attraction.html', {'form':attraction_form})

# browse contain a list of all attractions
def browse(request):
    attraction_list = Attraction.objects.all()
    response = render(request, 'planmytrip/browse.html', {'attractions': attraction_list})
    return response


def show_attraction(request, attraction_name_slug):
    context_dict = {}
    # get the attraction to display
    attraction = Attraction.objects.get(slug=attraction_name_slug)
    context_dict['attraction'] = attraction

    # if the user is authenticated create a review_form, if request
    # is post handle form submission
    if request.user.is_authenticated():
        user = request.user

        if request.method == 'POST':
            review_form = ReviewForm(request.POST)
            if review_form.is_valid():
                review = review_form.save(commit=False)
                review.user = user
                review.attraction = attraction
                review.save()
                return HttpResponseRedirect(reverse('show_attraction', args = (attraction.slug,)))
            else:
                print(review_form.errors)
        else:
            review_form = ReviewForm()
            context_dict['form'] = review_form

        # checks if the attraction was already liked
        profile = Profile.objects.get(user = user)
        attraction_liked = profile.liked_attractions.filter(slug=attraction_name_slug)
        context_dict['liked'] = attraction_liked.exists()

        # checks if user had already created a revied to dispay it
        try:
            context_dict['user_review'] = user.reviews.get(attraction=attraction)
        except Review.DoesNotExist:
            print("review does not exist")

    # make the average review box
    reviews_list = attraction.reviews.all()
    if reviews_list.exists() :
        appeal_avg = reviews_list.aggregate(Avg('appeal'))
        price_avg = reviews_list.aggregate(Avg('price'))
        eoa_avg = reviews_list.aggregate(Avg('ease_of_access'))
        round_appeal = {'appeal__avg' : round(appeal_avg['appeal__avg'],1)}
        round_price = {'price__avg' : round(price_avg['price__avg'],1)}
        round_eoa = {'ease_of_access__avg' : round(eoa_avg['ease_of_access__avg'],1)}
        review_avg = {}
        review_avg.update(round_appeal)
        review_avg.update(round_price)
        review_avg.update(round_eoa)
        context_dict['review_avg'] = review_avg
    else :
        context_dict['review_avg'] = None



    return render(request, 'planmytrip/attraction.html', context_dict)


# display user profile and its liked attractions
def show_user_profile(request):
    user = request.user
    profile = Profile.objects.get(user = user)
    attractions = profile.liked_attractions.all()
    response = render(request, 'planmytrip/profile.html',{"profile": profile,
                                                            "attractions" : attractions} )
    return response

# creates a page with a form for user registration
def registration(request):
    if request.method == 'POST':
        user_form = UserForm(data=request.POST)
        profile_form = ProfileForm(data=request.POST)

        if user_form.is_valid() and profile_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)


            if 'picture' in request.FILES:
                user.profile.picture = request.FILES['picture']

            user.save()
            return home(request)
        else:
            print(user_form.errors, profile_form.errors)

    else:
        user_form = UserForm()
        profile_form = ProfileForm()

    return render(request,
                  'planmytrip/registration.html',
                  {'user_form': user_form,
                   'profile_form': profile_form})

# respond to an AJAX request. Gives back all "choices" existing
# in the database for a particular attribute
def get_options(request):
    if request.method == 'GET':
        print('**get**')
        query = request.GET
        dictQuery = dict(query)
        option = dictQuery['option'][0].lower();
        attractions = Attraction.objects.values_list(option, flat=True)
        attractionList = list(attractions)
        attractionSet = set(attractionList)
        jsonobj = json.dumps(list(attractionSet))

        return JsonResponse(jsonobj, safe=False)

# respond to an AJAX request, sends back the url to load
def search_filter(request) :
    if request.is_ajax():
        queryDict = dict(request.GET)#filter type
        attr_filter = queryDict['option'][0].lower()
        value_filter = queryDict['filter'][0]
        return JsonResponse({
                'success': True,
                'url': reverse('filter_page', args = (attr_filter, value_filter) ),
            })
    return JsonResponse({ 'success': False })

# display the browse page with the filters applied
def filter_page(request, attr_filter, value_filter):
    specific_filter = {}
    specific_filter[attr_filter] = value_filter
    attraction_list = Attraction.objects.filter(**specific_filter)
    context_dict = {'attractions': attraction_list}
    return render(request, 'planmytrip/browse.html', context_dict)

# respond to an AJAX request for setting an attraction as liked
def like_attraction(request):
    query= request.GET
    liked_attraction = dict(query)
    attraction_name_slug = liked_attraction['liked_attraction'][0]
    user= request.user
    attraction = Attraction.objects.get(slug = attraction_name_slug)
    profile=Profile.objects.get(user=user)
    profile.liked_attractions.add(attraction)
    return JsonResponse({'success' : True})

# respond to an AJAX request for setting an attraction as unliked
def unlike_attraction(request):
    query= request.GET
    liked_attraction = dict(query)
    attraction_name_slug = liked_attraction['liked_attraction'][0]
    user= request.user
    attraction = Attraction.objects.get(slug = attraction_name_slug)
    profile=Profile.objects.get(user=user)
    profile.liked_attractions.remove(attraction)
    return JsonResponse({'success' : True})

# respond to an AJAX request. Gives back to JS an url to use and the
# arguments needed to display the browse page with the filter
def search_attraction(request):
    word_filter = request.GET.get('filter')
    return JsonResponse({
            'success': True,
            'url': reverse('search_page', args = (word_filter,)),
            })
    return JsonResponse({ 'success': False })

# applies the filter, create the right context_dict and display
# the browse page with the advanced filter
def search_page(request, word_filter):
    attraction_list = Attraction.objects.filter(name__icontains= word_filter)
    context_dict = {'attractions': attraction_list}
    return render(request, 'planmytrip/browse.html', context_dict)

# display the edit profile page with the form used to modify the profile
def edit_profile(request):
    profile = request.user.profile
    user = request.user
    edit_profile_form = EditProfileForm(request.POST or None, instance=profile)
    edit_user_form = EditUserForm(request.POST or None, instance=user)

    if request.method == 'POST':
        if edit_profile_form.is_valid() and edit_user_form.is_valid():
            user = edit_user_form.save()
            editprofile = edit_profile_form.save(commit = False)
            if 'picture' in request.FILES:
                editprofile.picture = request.FILES['picture']
            editprofile.save()
            return show_user_profile(request)

    context = {'edit_profile_form': edit_profile_form,
                'edit_user_form':edit_user_form}
    return render(request, 'planmytrip/edit_profile.html', context)

def about(request):
    return render(request, 'planmytrip/about.html', {})

def contact(request):
    return render(request, 'planmytrip/contact.html', {})

def faq(request):
    return render(request, 'planmytrip/faq.html', {})

# allow guests or user to see other users profile
def inspect_profile(request, username):
    print(username)
    user = User.objects.get(username=username)
    profile = user.profile
    attractions = profile.liked_attractions.all()
    return render(request, 'planmytrip/other_profile.html',{"profile": user,
                                                            "attractions" : attractions} )
