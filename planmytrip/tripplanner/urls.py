from django.conf.urls import url
from tripplanner import views

urlpatterns=[
    url(r'^$', views.home, name='home'),
    url(r'^about/$', views.about, name='about'),
    url(r'^contact/$', views.contact, name='contact'),
    url(r'^faq/$', views.faq, name='faq'),
    url(r'^browse/$', views.browse, name='browse'),
    url(r'^ajax/get_options/$', views.get_options, name='getOptions'),
    url(r'^attraction/(?P<attraction_name_slug>[\w\-]+)/$',
        views.show_attraction, name ='show_attraction'),
    url(r'^add_attraction/$', views.add_attraction, name='add_attraction'),
    url(r'^profile/$', views.show_user_profile, name='profile'),
    url(r'^registration/$', views.registration, name='registration'),
    url(r'^most_popular/$', views.most_popular, name='most_popular'),
    url(r'^ajax/search_filter/$', views.search_filter, name='search_filter'),
    url(r'^filter_page/(?P<attr_filter>.+?)/(?P<value_filter>.+?)/$', views.filter_page, name='filter_page'),
    url(r'^ajax/like_attraction/$', views.like_attraction, name='like_attraction'),
    url(r'^ajax/unlike_attraction/$', views.unlike_attraction, name='unlike_attraction'),
    url(r'^ajax/search_attraction/$', views.search_attraction, name='search_attraction'),
    url(r'^search_page/(?P<word_filter>.+?)/$', views.search_page, name='search_page'),
    url(r'^edit_profile/$', views.edit_profile, name='edit_profile'),
    url(r'^show_profile/(?P<username>[\w\-]+)/$', views.inspect_profile, name ='inspect_profile'),
]
