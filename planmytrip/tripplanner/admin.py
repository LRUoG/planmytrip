from django.contrib import admin
from tripplanner.models import Profile, Attraction, Review

class ReviewAdmin(admin.ModelAdmin):
    list_display = ('date','attraction')

admin.site.register(Profile)
admin.site.register(Attraction, )
admin.site.register(Review, ReviewAdmin)
