from django.contrib.auth.models import User
from django import forms
from tripplanner.models import Profile, Review, Attraction

# if you are making a custom user model, you need to change the import
# reference website: https://wsvincent.com/django-referencing-the-user-model/


class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ('username', 'password', 'first_name', 'last_name', 'email')


class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('picture',)

class EditProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['picture',]

class AttractionForm(forms.ModelForm):
    # position = forms.CharField(widget=forms.HiddenInput())
    slug = forms.CharField(widget=forms.HiddenInput(), required=False)
    region = forms.CharField(widget=forms.TextInput(attrs={'readonly':'readonly'}))
    province = forms.CharField(widget=forms.TextInput(attrs={'readonly':'readonly'}))
    description = forms.Textarea()
    # position = forms.CharField(widget=forms.TextInput(attrs={'readonly':'readonly'}))

    class Meta:
        model = Attraction
        fields = ('name', 'region', 'category', 'picture', 'province', 'position', 'description')

class EditUserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('username', 'email', 'first_name', 'last_name')

class ReviewForm(forms.ModelForm):
    #reviewID should be updated by server, not added by user
    text = forms.CharField(widget = forms.Textarea)
    class Meta:
        model = Review
        fields = ('appeal', 'price', 'ease_of_access', 'text')
