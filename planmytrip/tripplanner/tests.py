from django.test import TestCase
from tripplanner.models import Attraction, Review
from tripplanner.forms import AttractionForm
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User


# testing attraction model - run with "python manage.py test"
class ModelsTestCase(TestCase):
    # create an attraction object for testing
    def test_attraction_setUp(self, name="test attraction",
                                    region="some region",
                                    province="some province",
                                    category="nature",
                                    picture="Attractions/test.jpg",
                                    position="0, 0",
                                    description="test some description",
                                    slug="test-attraction"):
        return Attraction.objects.create(name=name,
                                        region=region,
                                        province=province,
                                        category=category,
                                        picture=picture,
                                        position=position,
                                        description=description,
                                        slug=slug)

    # test if the attraction was created
    def test_attraction_creation(self):
        # set up the attraction
        a = self.test_attraction_setUp()
        # check if it is an instance of the model Attraction
        self.assertTrue(isinstance(a, Attraction))
        # check that an object of the same name was created
        self.assertEqual(a.__str__(), a.name)


    #This will test if added attraction displays on front page
    def test_attraction_picture_display(self):
        # set up the attraction
        a = self.test_attraction_setUp()
        #access front HomePage
        response = self.client.get(reverse('home'))

        #check if photo appears on front page
        self.assertIn('src="/media/Attractions/test.jpg"', response.content.decode('ascii'))

# create a moc user on call
    def test_user_setUp(self, username='testUser', password='supersecret'):
        return User.objects.create(username=username, password=password)

    def test_review_creation(self):
        a = self.test_attraction_setUp()

        # setup a random review
        r = Review.objects.create(date="00-00-0000",
                            text="test review text",
                            attraction=a,
                            user=self.test_user_setUp(),
                            appeal="5", price="2", ease_of_access="3")
        # check if it is an instance of review model
        self.assertTrue(isinstance(r, Review))
        # check if the object created corresponds in text
        self.assertEqual(r.__str__(), r.text)


# test that the review gets shown on the attraction page
    def test_review_displays(self):
        a = self.test_attraction_setUp()
        r = Review.objects.create(date="00-00-0000",
                            text="test review text",
                            attraction=a,
                            user=self.test_user_setUp(),
                            appeal="5", price="2", ease_of_access="3")

        # access the page with the attraction we created
        response = self.client.get(reverse('show_attraction', args=(a.slug,)))

        # check if the review we created is on the attraction page
        self.assertIn(r.text, response.content.decode('ascii'))


#Check if static social login icons visible
class SocialLogin(TestCase):
    def test_social_picture(self):
        response = self.client.get(reverse('home'))
        #check if facebook icon appears
        self.assertIn('img src="/static/images/facebook-icon.png"', response.content.decode('ascii'))
        #check if google icon appears
        self.assertIn('img src="/static/images/google-icon.png"', response.content.decode('ascii'))


# due to the recommendation to not use selenium, it was not possible to test logged in content
# the test methods were left in for presentation, albeti they are not used
class ReverseTestCases(TestCase):
    def test_page_titles(self):

        #Access homepage and check that its title is displayed
        response = self.client.get(reverse('home'))
        self.assertIn('HomePage', response.content.decode('ascii'))

        #Access browse page and check that its title is displayed
        response = self.client.get(reverse('browse'))
        self.assertIn('Browse', response.content.decode('ascii'))

        #Access most popular and check that its title is displayed
        response = self.client.get(reverse('most_popular'))
        self.assertIn('Popular', response.content.decode('ascii'))

    #    response = self.client.get(reverse('attraction', args=[attraction[0].slug]))
    #    self.assertIn(attraction[0].name.lower(), response.content.decode('ascii').lower())

    #   This could not be tested as we are not using selenium tests, and adding attraction requires login
    #    response = self.client.get(reverse('add_attraction'))
    #    self.assertIn('Add Attraction', response.content.decode('ascii'))

        #Access login page and check that its title is displayed
        response = self.client.get(reverse('login'))
        self.assertIn('Login', response.content.decode('ascii'))

        #Access registration page and check that its title is displayed
        response = self.client.get(reverse('registration'))
        self.assertIn('Registration', response.content.decode('ascii'))

    #   Could not be tested, as to access profile page, a user name is required
    #   response = self.client.get(reverse('profile'))
    #   self.assertIn(user.username, response.content.decode('ascii'))
    #   this checks a static title hence it is possible to test
    #   self.assertIn('Profile', response.content.decode('ascii'))

    #   Access edit_profile page and check that its title is displayed
    #   response = self.client.get(reverse('editprofile'))
    #   self.assertIn('Edit Profile', response.content.decode('ascii'))

        #Access about page and check that its title is displayed
        response = self.client.get(reverse('about'))
        self.assertIn('About', response.content.decode('ascii'))

        #Access contact us page and check that its title is displayed
        response = self.client.get(reverse('contact'))
        self.assertIn('Contact Us', response.content.decode('ascii'))

        #Access faq page and check that its title is displayed
        response = self.client.get(reverse('faq'))
        self.assertIn('FAQ', response.content.decode('ascii'))

    # Test links in base template
    def test_working_urls_in_base(self):
        #First, access the front homepage
        response = self.client.get(reverse('home'))

        #Test that links exist and redirect correctly
        self.assertIn(reverse('browse'), response.content.decode('ascii'))
        self.assertIn(reverse('most_popular'), response.content.decode('ascii'))
        self.assertIn(reverse('login'), response.content.decode('ascii'))
        self.assertIn(reverse('about'), response.content.decode('ascii'))
        self.assertIn(reverse('faq'), response.content.decode('ascii'))
        self.assertIn(reverse('contact'), response.content.decode('ascii'))

    def test_register_link(self):
        #Access front HomePage
        response = self.client.get(reverse('home'))

        #Test sign up link
        self.assertIn(reverse('registration'), response.content.decode('ascii'))
