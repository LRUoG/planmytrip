import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE',
 'planmytrip.settings')

import django

django.setup()
from tripplanner.models import Attraction, Review, Profile
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password
from geoposition.fields import GeopositionField
from geoposition import Geoposition

def populate():


    history = [
            {"name":"Colosseum",
                "region":"Lazio",
                "picture":"attractions/Colosseum.jpg",
                "province" : "Provincia di Roma",
                "position": [41.8899435,12.494337099999939],
                "description":'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Hendrerit gravida rutrum quisque non tellus orci ac. Risus feugiat in ante metus dictum at tempor commodo. Tincidunt arcu non sodales neque sodales ut etiam. Tortor condimentum lacinia quis vel eros donec ac. Lectus vestibulum mattis ullamcorper velit. Commodo viverra maecenas accumsan lacus vel. Non nisi est sit amet facilisis magna etiam tempor. Cursus sit amet dictum sit amet justo donec. Porttitor leo a diam sollicitudin tempor. Sed cras ornare arcu dui vivamus.'},
            {"name":"St Peter's Basilica",
                "region":"Puglia",
                "picture":"attractions/St peter's basilica.jpg",
                "province" : "Provincia di Roma",
                "position": [41.9022225,12.456728300000009],
                "description":'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Hendrerit gravida rutrum quisque non tellus orci ac. Risus feugiat in ante metus dictum at tempor commodo. Tincidunt arcu non sodales neque sodales ut etiam. Tortor condimentum lacinia quis vel eros donec ac. Lectus vestibulum mattis ullamcorper velit. Commodo viverra maecenas accumsan lacus vel. Non nisi est sit amet facilisis magna etiam tempor. Cursus sit amet dictum sit amet justo donec. Porttitor leo a diam sollicitudin tempor. Sed cras ornare arcu dui vivamus.'},
            {"name":"Trevi Fountain",
                "region":"Lazio",
                "picture":"attractions/Trevi.jpg",
                "province" : "Provincia di Padova",
                "position": [41.9007617,12.483313299999963],
                "description":'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Hendrerit gravida rutrum quisque non tellus orci ac. Risus feugiat in ante metus dictum at tempor commodo. Tincidunt arcu non sodales neque sodales ut etiam. Tortor condimentum lacinia quis vel eros donec ac. Lectus vestibulum mattis ullamcorper velit. Commodo viverra maecenas accumsan lacus vel. Non nisi est sit amet facilisis magna etiam tempor. Cursus sit amet dictum sit amet justo donec. Porttitor leo a diam sollicitudin tempor. Sed cras ornare arcu dui vivamus.'}
                ]

    sport = [
            {"name":"San Siro",
                "region":"Lombardia",
                "picture":"attractions/sansiro.jpg",
                "province" : "Provincia di Como",
                "position": [45.4765938,9.122809400000051],
                "description":'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Hendrerit gravida rutrum quisque non tellus orci ac. Risus feugiat in ante metus dictum at tempor commodo. Tincidunt arcu non sodales neque sodales ut etiam. Tortor condimentum lacinia quis vel eros donec ac. Lectus vestibulum mattis ullamcorper velit. Commodo viverra maecenas accumsan lacus vel. Non nisi est sit amet facilisis magna etiam tempor. Cursus sit amet dictum sit amet justo donec. Porttitor leo a diam sollicitudin tempor. Sed cras ornare arcu dui vivamus.'},
            {"name":"Stadio delle Alpi",
                "region":"Piemonte",
                "picture":"attractions/stadio-delle-alpi.jpg",
                "province" : "Provincia di Torino",
                "position": [45.1053146,7.635331700000052],
                "description":'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Hendrerit gravida rutrum quisque non tellus orci ac. Risus feugiat in ante metus dictum at tempor commodo. Tincidunt arcu non sodales neque sodales ut etiam. Tortor condimentum lacinia quis vel eros donec ac. Lectus vestibulum mattis ullamcorper velit. Commodo viverra maecenas accumsan lacus vel. Non nisi est sit amet facilisis magna etiam tempor. Cursus sit amet dictum sit amet justo donec. Porttitor leo a diam sollicitudin tempor. Sed cras ornare arcu dui vivamus.'}
                ]

    nature = [
            {"name":"Baia Riaci",
                "region":"Calabria",
                "picture":"attractions/Riaci.jpg",
                "province" : "Provincia di Sassari",
                "position": [38.6701521,15.870842000000039],
                "description":'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Hendrerit gravida rutrum quisque non tellus orci ac. Risus feugiat in ante metus dictum at tempor commodo. Tincidunt arcu non sodales neque sodales ut etiam. Tortor condimentum lacinia quis vel eros donec ac. Lectus vestibulum mattis ullamcorper velit. Commodo viverra maecenas accumsan lacus vel. Non nisi est sit amet facilisis magna etiam tempor. Cursus sit amet dictum sit amet justo donec. Porttitor leo a diam sollicitudin tempor. Sed cras ornare arcu dui vivamus.'},
            {"name":"Spiaggia dei Conigli",
                "region":"Sicilia",
                "picture":"attractions/spiaggia-dei-conigli.jpg",
                "province" : "Provincia di Cagliari",
                "position": [35.5074087,12.604399599999965],
                "description":'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Hendrerit gravida rutrum quisque non tellus orci ac. Risus feugiat in ante metus dictum at tempor commodo. Tincidunt arcu non sodales neque sodales ut etiam. Tortor condimentum lacinia quis vel eros donec ac. Lectus vestibulum mattis ullamcorper velit. Commodo viverra maecenas accumsan lacus vel. Non nisi est sit amet facilisis magna etiam tempor. Cursus sit amet dictum sit amet justo donec. Porttitor leo a diam sollicitudin tempor. Sed cras ornare arcu dui vivamus.'}
                ]

    reviews = [{"text":"Fantastic place mate",
                "attraction": "Colosseum",
                "user":"Loris",
                "appeal":4,
                "price":3,
                "ease_of_access":5},
                {"text":"Not too bad",
                "attraction": "Colosseum",
                "user":"Emily",
                "appeal":4,
                "price":3,
                "ease_of_access":5},
                {"text":"Fantastic place mate",
                "attraction": "Spiaggia dei Conigli",
                "user":"Stefanos",
                "appeal":4,
                "price":3,
                "ease_of_access":5},
                {"text":"Not so good place, dont go",
                "attraction": "Trevi Fountain",
                 "user":"Stefanos",
                 "appeal":3,
                 "price":1,
                 "ease_of_access":4}]


    profiles = [{"name":"Loris",
                "lastname":"Rodigari",
                "picture":"profile/prof.jpg",
                "password":"12345!",
                "username":"heyguy",
                "email":"lorisrodigari@gmail.com"},
                {"name":"Stefanos",
                "lastname":"Sotiriou",
                "picture":"profile/prof.jpg",
                "password":"gage029coin022",
                "username":"thisguy",
                "email":"sotirioustef@gmail.com"},
                {"name":"Emily",
                "lastname":"Hosokawa",
                "picture":"profile/prof.jpg",
                "password":"qwerty",
                "username":"emyDot",
                "email":"emyemy@gmail.com"}]


    cats = {"history":{"category":history},
            "sport":{"category":sport},
            "nature":{"category":nature}}

    for cat,cat_data in cats.items():
        c_dictionary = cat_data["category"]
        for c in c_dictionary:
            a = add_attraction(cat, c["name"], c["region"], c["picture"], c["province"], c["position"], c["description"])

    for myprof in profiles:
        p= add_profile(myprof["name"], myprof["lastname"], myprof["picture"], myprof["password"], myprof["username"], myprof["email"])

    for myrev in reviews:
        r= add_review(myrev["text"], myrev["attraction"], myrev["user"], myrev["appeal"], myrev["price"], myrev["ease_of_access"])


    for r in Review.objects.all():
        print("{0}",format(str(r)))

    for a in Attraction.objects.all():
        print("{0}",format(str(a)))

    for p in Profile.objects.all():
        print("{0}",Profile.user)



def add_attraction(category, name, region, picture, province, pos, description):
    a= Attraction.objects.get_or_create(name=name)[0]
    a.region=region
    a.picture=picture
    a.category=category
    a.position = Geoposition(pos[0],pos[1])
    a.province= province
    a.description=description
    a.save()
    return a

def add_profile(name, lastname, picture, password, username, e_mail):
    attraction1 = Attraction.objects.get(name="Trevi Fountain")
    mypassword= make_password(password, salt=None, hasher='default')
    user= User.objects.create(username=username, password=mypassword, first_name=name, last_name=lastname, email=e_mail)
    p= Profile.objects.get_or_create(user=user)[0]
    p.liked_attractions.add(attraction1)
    p.picture=picture
    p.save()
    return p

def add_review(text, attraction_name, user_name, appeal, price, ease_of_access):
    attraction = Attraction.objects.get(name=attraction_name)
    user = User.objects.get(first_name=user_name)
    r = Review.objects.get_or_create(attraction= attraction, user= user)[0]
    r.text=text
    r.appeal=appeal
    r.price=price
    r.ease_of_access=ease_of_access
    r.save()
    return r

if __name__ == '__main__':
    print("Starting Plantrip population script...")
    populate()
